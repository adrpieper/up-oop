package oop.osoba;

public class Osoba {
	//Stan, dane s� ukryte przed u�ytkownikiem klasy
	private String imie;
	private int wiek;

	public Osoba() {
		
	}
	
	//Konstruktor
	public Osoba(String imie, int wiek) {
		this.imie = imie;
		this.wiek = wiek;
	}
	
	//Zachowanie
	public void przedstawSie() {
		System.out.println(imie + " (" + this.wiek + ")");
	}
	
	public void setImie(String imie) {
		this.imie = imie;
	}
	
	public void setWiek(int wiek) {
		this.wiek = wiek;
	}
	
	public String getImie() {
		return imie;
	}
	
	public int getWiek() {
		return wiek;
	}
	
}
