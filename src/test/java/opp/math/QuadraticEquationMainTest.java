package opp.math;

import org.junit.Test;

public class QuadraticEquationMainTest {

	@Test
	public void testDelta() {

		QuadraticEquation equation = new QuadraticEquation(1, 4, 3);
		assert equation.calcDelta() == 4.0;
	}
	
	@Test
	public void testX1() {
		QuadraticEquation equation = new QuadraticEquation(1, 4, 3);
		assert equation.calcX1() == -3.0;
	}
	
	@Test
	public void testX2() {

		QuadraticEquation equation = new QuadraticEquation(1, 4, 3);
		assert equation.calcX2() == -1.0;
	}

}
